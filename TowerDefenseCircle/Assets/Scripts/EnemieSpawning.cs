﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemieSpawning : MonoBehaviour
{
    private CircleCollider2D spawnRadius;
    public DifficultySettings Settings;

    [Header("EnemyPrefabs")]
    [SerializeField] private GameObject blockBoiPrefab;
    [SerializeField] private GameObject orbitalBoiPrefab;
    [SerializeField] private GameObject messengerBoiPrefab;
    [SerializeField] private GameObject bigBoiPrefab;
    [SerializeField] private GameObject lineBackerPrefab;


    [Header("SpawnTimes")]
    [SerializeField] private float blockBoiSpawnTime;
    [SerializeField] private float orbitalBoiSpawnTime;
    [SerializeField] private float messengerBoiSpawnTime;
    [SerializeField] private float lineBackerBoiSpawnTime;

    private float blockBoiMinSpawnTime;
    private float orbitalBoiMinSpawnTime;
    private float messengerBoiMinSpawnTime;
    private float lineBackerBoiMinSpawnTime;

    private float blockBoiCurrentSpawnTime;
    private float orbitalBoiCurrentSpawnTime;
    private float messengerBoiCurrentSpawnTime;
    private float lineBackerBoiCurrentSpawnTime;

    [Header("FirstAppearance")]
    [SerializeField] private float initialBlockBoi;
    [SerializeField] private float initialOrbitalBoi;
    [SerializeField] private float initialMessengerBoi;
    [SerializeField] private float initialLineBackerBoi;


    private List<GameObject> blockBois = new List<GameObject>();
    private List<GameObject> orbitalBois = new List<GameObject>();
    private List<GameObject> messengerBois = new List<GameObject>();
    private List<GameObject> bigBois = new List<GameObject>();
    private List<GameObject> LineBackerBois = new List<GameObject>();

    private int blockBoisPoolSize = 50;
    private int orbitalBoisPoolSize = 50;
    private int messengerBoisPoolSize = 50;
    private int bigBoisPoolSize = 50;
    private int lineBackerPoolSize = 50;

    [Header("Difficulty")]
    public float difficultyIncrease;
    public float timeStepTillIncrease;
    private float timer;
    private float inGameTime;

    private void OnEnable()
    {
        ApplySettings();
    }

    // Start is called before the first frame update
    void Start()
    {
        spawnRadius = GetComponent<CircleCollider2D>();
        CreatePools();

        blockBoiCurrentSpawnTime += blockBoiSpawnTime;
        orbitalBoiCurrentSpawnTime += orbitalBoiSpawnTime;
        messengerBoiCurrentSpawnTime += messengerBoiSpawnTime;
        lineBackerBoiCurrentSpawnTime += lineBackerBoiSpawnTime;

        //InvokeRepeating("SpawnBlockBoi", 0f, blockBoiSpawnTime);
        //InvokeRepeating("SpawnOrbitalBoi", 2f, orbitalBoiSpawnTime);
        //InvokeRepeating("SpawnMessengerBoi", 4f, messengerBoiSpawnTime);
        //InvokeRepeating("SpawnLineBackerBoi", 6f, lineBackerBoiSpawnTime);
        //InvokeRepeating("SpawnSpecials", 1f, 2f);
    }

    private void Update()
    {
        inGameTime += Time.deltaTime;
        timer += Time.deltaTime;
        if (timer > timeStepTillIncrease)
        {
            timer = 0;
            CalculateSpawnTimers();
        }
        SpawnEnemies();
    }

    public void GameLost()
    {
        for (int i = 0; i < blockBois.Count; i++)
        {
            blockBois[i].SetActive(false);
            messengerBois[i].SetActive(false);
            orbitalBois[i].SetActive(false);
            bigBois[i].SetActive(false);
            LineBackerBois[i].SetActive(false);

        }
    }

    private void ApplySettings()
    {
        if (Settings.isOnePlayerMode)
        {
            blockBoiMinSpawnTime = Settings.minBlockBoiSpawnTime;
            orbitalBoiMinSpawnTime = Settings.minOrbitalBoiSpawnTime;
            messengerBoiMinSpawnTime = Settings.minMessengerBoiSpawnTime;
            lineBackerBoiMinSpawnTime = Settings.minLineBackerBoiSpawnTime;

            blockBoiSpawnTime = Settings.initialBlockBoiSpawnTime;
            orbitalBoiSpawnTime = Settings.initialOrbitalBoiSpawnTime;
            messengerBoiSpawnTime = Settings.initialMessengerBoiSpawnTime;
            lineBackerBoiSpawnTime = Settings.initialLineBackerBoiSpawnTime;

            difficultyIncrease = Settings.difficultyIncreasePercentage;
            timeStepTillIncrease = Settings.difficultyTimeTillIncrease;
        }
        else
        {
            blockBoiMinSpawnTime = Settings.minBlockBoiSpawnTimeTwo;
            orbitalBoiMinSpawnTime = Settings.minOrbitalBoiSpawnTimeTwo;
            messengerBoiMinSpawnTime = Settings.minMessengerBoiSpawnTimeTwo;
            lineBackerBoiMinSpawnTime = Settings.minLineBackerBoiSpawnTimeTwo;

            blockBoiSpawnTime = Settings.initialBlockBoiSpawnTimeTwo;
            orbitalBoiSpawnTime = Settings.initialOrbitalBoiSpawnTimeTwo;
            messengerBoiSpawnTime = Settings.initialMessengerBoiSpawnTimeTwo;
            lineBackerBoiSpawnTime = Settings.initialLineBackerBoiSpawnTimeTwo;


            difficultyIncrease = Settings.difficultyIncreasePercentageTwo;
            timeStepTillIncrease = Settings.difficultyTimeTillIncreaseTwo;
        }
    }
    private void CalculateSpawnTimers()
    {
        blockBoiSpawnTime = Mathf.Max(blockBoiSpawnTime - (blockBoiSpawnTime / 100) * difficultyIncrease, blockBoiMinSpawnTime);
        orbitalBoiSpawnTime = Mathf.Max(orbitalBoiSpawnTime - (orbitalBoiSpawnTime / 100) * difficultyIncrease, orbitalBoiMinSpawnTime);
        messengerBoiSpawnTime = Mathf.Max(messengerBoiSpawnTime - (messengerBoiSpawnTime / 100) * difficultyIncrease, messengerBoiMinSpawnTime);
        lineBackerBoiSpawnTime = Mathf.Max(lineBackerBoiSpawnTime - (lineBackerBoiSpawnTime / 100) * difficultyIncrease, lineBackerBoiMinSpawnTime);
    }

    private void SpawnEnemies()
    {
        blockBoiCurrentSpawnTime += Time.deltaTime;
        orbitalBoiCurrentSpawnTime += Time.deltaTime;
        messengerBoiCurrentSpawnTime += Time.deltaTime;
        lineBackerBoiCurrentSpawnTime += Time.deltaTime;

        if (blockBoiCurrentSpawnTime > blockBoiSpawnTime && inGameTime >= initialBlockBoi)
        {
            blockBoiCurrentSpawnTime = 0;
            SpawnBlockBoi();
        }
        if (orbitalBoiCurrentSpawnTime > orbitalBoiSpawnTime && inGameTime >= initialOrbitalBoi)
        {
            orbitalBoiCurrentSpawnTime = 0;
            SpawnOrbitalBoi();
        }
        if (messengerBoiCurrentSpawnTime > messengerBoiSpawnTime && inGameTime >= initialMessengerBoi)
        {
            messengerBoiCurrentSpawnTime = 0;
            SpawnMessengerBoi();
        }
        if (lineBackerBoiCurrentSpawnTime > lineBackerBoiSpawnTime && inGameTime >= initialLineBackerBoi)
        {
            lineBackerBoiCurrentSpawnTime = 0;
            SpawnLineBackerBoi();
        }
    }


    #region UnitSpawn and Pool
    public void SpawnBigBoi(Vector3 spawnPos)
    {
        for (int i = 0; i < bigBois.Count; i++)
        {
            if (!bigBois[i].activeSelf)
            {
                bigBois[i].transform.position = spawnPos;
                bigBois[i].SetActive(true);
                break;
            }
        }
    }

    private void SpawnBlockBoi()
    {
        Vector3 tempPos = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        tempPos.Normalize();
        tempPos *= spawnRadius.radius;
        for (int i = 0; i < blockBois.Count; i++)
        {
            if (!blockBois[i].activeSelf)
            {
                blockBois[i].transform.position = tempPos;
                blockBois[i].SetActive(true);
                break;
            }
        }
    }

    private void SpawnOrbitalBoi()
    {
        Vector3 tempPos = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        tempPos.Normalize();
        tempPos *= spawnRadius.radius;
        for (int i = 0; i < orbitalBois.Count; i++)
        {
            if (!orbitalBois[i].activeSelf)
            {
                orbitalBois[i].transform.position = tempPos;
                orbitalBois[i].SetActive(true);
                break;
            }
        }
    }

    private void SpawnMessengerBoi()
    {
        Vector3 tempPos = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        tempPos.Normalize();
        tempPos *= spawnRadius.radius;
        for (int i = 0; i < messengerBois.Count; i++)
        {
            if (!messengerBois[i].activeSelf)
            {
                messengerBois[i].transform.position = tempPos;
                messengerBois[i].SetActive(true);
                break;
            }
        }
    }

    private void SpawnLineBackerBoi()
    {

        Vector3 tempPos = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        tempPos.Normalize();
        tempPos *= spawnRadius.radius;
        for (int i = 0; i < LineBackerBois.Count; i++)
        {
            if (!LineBackerBois[i].activeSelf)
            {
                LineBackerBois[i].transform.position = tempPos;
                LineBackerBois[i].SetActive(true);
                break;
            }
        }
    }
    private void CreatePools()
    {
        var container = new GameObject("EnemyPool");
        for (int i = 0; i < blockBoisPoolSize; i++)
        {
            GameObject temp = Instantiate(blockBoiPrefab, new Vector3(1 * i, 10, 0), Quaternion.identity);
            blockBois.Add(temp);
            temp.SetActive(false);
            temp.transform.parent = container.transform;
        }
        for (int i = 0; i < orbitalBoisPoolSize; i++)
        {
            GameObject temp = Instantiate(orbitalBoiPrefab, new Vector3(1 * i, 15, 0), Quaternion.identity);
            orbitalBois.Add(temp);
            temp.SetActive(false);
            temp.transform.parent = container.transform;
        }
        for (int i = 0; i < messengerBoisPoolSize; i++)
        {
            GameObject temp = Instantiate(messengerBoiPrefab, new Vector3(1 * i, 20, 0), Quaternion.identity);
            messengerBois.Add(temp);
            temp.SetActive(false);
            temp.transform.parent = container.transform;
        }
        for (int i = 0; i < bigBoisPoolSize; i++)
        {
            GameObject temp = Instantiate(bigBoiPrefab, new Vector3(1 * i, 25, 0), Quaternion.identity);
            bigBois.Add(temp);
            temp.SetActive(false);
            temp.transform.parent = container.transform;
        }
        for (int i = 0; i < lineBackerPoolSize; i++)
        {
            GameObject temp = Instantiate(lineBackerPrefab, new Vector3(1 * i, 30, 0), Quaternion.identity);
            LineBackerBois.Add(temp);
            temp.SetActive(false);
            temp.transform.parent = container.transform;
        }
    }
    #endregion

}
