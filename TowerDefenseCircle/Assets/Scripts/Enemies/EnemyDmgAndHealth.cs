﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDmgAndHealth : MonoBehaviour
{
    [SerializeField] private int attackPower;
    [SerializeField] private int health;
    private Animator anim;
    private int baseHealth;
    private void Start()
    {
        baseHealth = health;
        anim = GetComponent<Animator>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Base"))
        {
            collision.gameObject.GetComponent<BaseBehavior>().Damage(attackPower);
            gameObject.SetActive(false);
        }
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Damage(1);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Projectile"))
        {
            Damage(1);
        }
    }

    public virtual void Damage(int amount)
    {
        anim.SetTrigger("Damage");
        health -= amount;
        if (health <= 0)
        {
            gameObject.SetActive(false);
            health = baseHealth;
        }
    }

}
