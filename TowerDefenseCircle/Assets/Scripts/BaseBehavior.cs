﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BaseBehavior : MonoBehaviour
{
    [SerializeField] private int health;
    [SerializeField] private TMP_Text healthText;
    private Animator anim;
    private AudioSource source;

    private void Start()
    {
        anim = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
        DisplayHealth();
    }

    public void Damage(int amount)
    {
        if (amount > 0)
        {
            anim.SetTrigger("Damage");
            source.Play();
            
        }
        health -= amount;
        DisplayHealth();
        if (health <= 0)
        {
            GameManager.Instance.GameLost();
        }
    }

    private void DisplayHealth()
    {
        string temp = "";
        for (int i = 0; i < health; i++)
        {
            temp += "x ";
        }
        healthText.text = temp;
    }
}
