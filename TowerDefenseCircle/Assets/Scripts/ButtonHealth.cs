﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHealth : MonoBehaviour
{
    public enum FunctiontoCall
    {
        Start,Quit,Settings,Reset
    }
    [SerializeField] private int health;
    private bool isDead;
    [SerializeField] private FunctiontoCall function;
    private void Damage()
    {
        health--;
        if (health <= 0)
        {
            health = 0;
            switch (function)
            {
                case FunctiontoCall.Start:
                    GameManager.Instance.StartGame();
                    break;
                case FunctiontoCall.Quit:
                    GameManager.Instance.QuitGame();
                    break;
                case FunctiontoCall.Settings:
                    break;
                default:
                    break;
            }
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Projectile"))
        {
            Damage();
        }
    }


}
