﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using SinputSystems;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    private GameObject spawner;
    [SerializeField] private HighScore singlePlayerHighScore;
    [SerializeField] private HighScore multiPlayerHighScore;
    [SerializeField] private TMP_Text currentScoreText;
    [SerializeField] private DifficultySettings spawnSettings;

    [SerializeField] private GameObject PlayerOne;
    [SerializeField] private GameObject PlayerTwo;

    private bool gameIsRunning = true;
    private float currentMilliSecs;
    private int currentSeconds;
    private int currentMinutes;
    [SerializeField] private GameObject ScoreNamesObj;
    private Vector3 lastMousePos;
    private float mouseVisibilityTimer;
    public List<ScoreEntry> singles;
    public List<ScoreEntry> multies;
    HighScoreData data;

    void Start()
    {
        data = SaveSystem.LoadScores();
        if (data == null)
        {
            singles = new List<ScoreEntry>();
            multies = new List<ScoreEntry>();
        }
        spawner = GameObject.FindGameObjectWithTag("Spawner");
        spawner.SetActive(false);
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        StartGame();
    }

    void Update()
    {
        if (lastMousePos != Input.mousePosition)
        {
            Cursor.visible = true;
            mouseVisibilityTimer = 0;
        }
        else
        {
            mouseVisibilityTimer += Time.deltaTime;
            if (mouseVisibilityTimer >= 1)
            {
                Cursor.visible = false;
            }
        }
        lastMousePos = Input.mousePosition;

        if (gameIsRunning)
        {
            CalculateTime();
            SetScoreText();
        }

    }

    public void GameLost()
    {
        gameIsRunning = false;
        spawner.GetComponent<EnemieSpawning>().GameLost();
        spawner.SetActive(false);
        ScoreNamesObj.SetActive(true);
        PlayerOne.SetActive(false);
        PlayerTwo.GetComponent<TowerShooting>().enabled = false;
        PlayerTwo.GetComponent<TowerMovement>().enabled = false;
    }
    public void StartGame()
    {
        spawnSettings.isOnePlayerMode = !PlayerTwo.activeSelf;
        spawner.SetActive(true);
        gameIsRunning = true;
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    private void CalculateTime()
    {
        currentMilliSecs += Time.deltaTime;
        if (currentMilliSecs > 1f)
        {
            currentMilliSecs = 0;
            currentSeconds++;
            if (currentSeconds % 60 == 0)
            {
                currentSeconds = 1;
                currentMinutes++;
            }
        }
    }

    private void SetScoreText()
    {
        float roundedMillis = (float)(Math.Truncate((double)currentMilliSecs * 1000.0) / 1000.0);
        float _scoretextboi = currentMinutes * 60 + currentSeconds + roundedMillis;
        currentScoreText.text = "" + _scoretextboi;
    }

    //[ContextMenu("RandomFilL")]
    //private void RandomFill()
    //{
    //    singlePlayerHighScore.HighScores.Clear();
    //    for (int i = 0; i < 5; i++)
    //    {
    //        singlePlayerHighScore.HighScores.Add(new ScoreEntry("Player" + i, UnityEngine.Random.Range(1, 500)));
    //    }
    //}
    public void SetHighScore(string playerName)
    {
        float roundedMillis = (float)(Math.Truncate((double)currentMilliSecs * 1000.0) / 1000.0);

        float _score = (roundedMillis + currentSeconds + currentMinutes * 60);
        if (!PlayerTwo.activeSelf)
        {
            if (data != null)
            {
                singles = data.SinglePlayerScores;
                multies = data.MultiplayerScores;
            }
            singles.Add(new ScoreEntry(playerName, _score));
            singles.Sort(SortByScore);

            SaveSystem.SaveScores(this);
        }
        else
        {
            if (data != null)
            {
                multies = data.MultiplayerScores;
                singles = data.SinglePlayerScores;
            }
            multies.Add(new ScoreEntry(playerName, _score));
            multies.Sort(SortByScore);

            SaveSystem.SaveScores(this);
        }
        SceneManager.LoadScene(0);

    }

    static int SortByScore(ScoreEntry p1, ScoreEntry p2)
    {
        return p1.score.CompareTo(p2.score) * -1;
    }
}
