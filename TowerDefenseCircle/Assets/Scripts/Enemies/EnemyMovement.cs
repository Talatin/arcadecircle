﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    private Rigidbody2D rb;
    [SerializeField] private float speed;
    [SerializeField] private bool isOrbital;
    [SerializeField] private float rotateSpeed;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void FixedUpdate()
    {

        rb.velocity = (Vector3.zero - transform.position).normalized * speed * Time.fixedDeltaTime;
        if (isOrbital)
            transform.RotateAround(Vector3.zero, Vector3.forward, rotateSpeed * Time.fixedDeltaTime);
    }
    private void OnEnable()
    {

    }

}
