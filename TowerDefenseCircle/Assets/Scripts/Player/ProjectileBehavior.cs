﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehavior : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        if (Vector2.Distance(transform.position,Vector2.zero) > 9)
        {
            gameObject.SetActive(false);
        }
    }
}
