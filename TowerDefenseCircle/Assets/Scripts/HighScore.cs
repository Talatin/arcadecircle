﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class HighScore : ScriptableObject
{
    public List<ScoreEntry> HighScores = new List<ScoreEntry>();
}

[System.Serializable]
public class ScoreEntry
{
    public string name;
    public float score;

     public ScoreEntry(string _name,float _score)
    {
        name = _name;
        score = _score;
    }

    public ScoreEntry()
    {

    }
}
