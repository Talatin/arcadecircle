﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenue : MonoBehaviour
{
    public EventSystem eveSys;
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private AudioSaveFile audioSave;
    [Header("UI Objects")]
    [SerializeField] private GameObject[] menuObjects;
    [SerializeField] private GameObject[] SettingsObjects;
    [SerializeField] private GameObject[] audioObjects;
    [SerializeField] private GameObject[] audioSliderObjects;
    [Header("Highscores")]
    public List<ScoreEntry> singlePlayerHighScore;
    public List<ScoreEntry> multiPlayerHighScore;
    [Space]
    [SerializeField] private ScrollRect singlePlayerRect;
    [SerializeField] private RectTransform singlePlayerRectTransform;
    [SerializeField] private ScrollRect multiPlayerRect;
    [SerializeField] private RectTransform multiPlayerRectTransform;
    [SerializeField] private float scrollSpeed;
    [SerializeField] private GameObject[] singlePlayerEntries;
    [SerializeField] private GameObject[] multiPlayerEntries;
    [Header("Sounds")]
    [SerializeField] private AudioClip hoverClip;
    [SerializeField] private AudioClip clickClip;

    private AudioSource source;
    private bool scrollSinglePlayer;
    private bool scrollMultiPlayer;
    private float yAxis;
    private Vector3 lastMousePos;
    private float mouseVisibilityTimer;
    public float[] audioSliderValue;
    public int ResolutionID;
    public int ScreenModeEnum;
    private MenuSaveData data;
    private HighScoreData scoreData;
    private void Awake()
    {
        data = null;
        audioSliderValue = new float[3];
        source = GetComponent<AudioSource>();
        data = SaveSystem.LoadPlayer();
        scoreData = SaveSystem.LoadScores();
        if (data != null)
        {
            audioSliderValue[0] = data.Volumes[0];
            audioSliderValue[1] = data.Volumes[1];
            audioSliderValue[2] = data.Volumes[2];
        }
        else
        {
            audioSliderValue[0] = 1f;
            audioSliderValue[1] = 0.5f;
            audioSliderValue[2] = 0.3f;
        }

        if (scoreData != null)
        {
            singlePlayerHighScore = scoreData.SinglePlayerScores;
            multiPlayerHighScore = scoreData.MultiplayerScores;
        }
        else
        {
            multiPlayerHighScore = new List<ScoreEntry>();
            singlePlayerHighScore = new List<ScoreEntry>();
        }

        SetVolume(0);
        SetVolume(1);
        SetVolume(2);

        audioSliderObjects[0].GetComponent<Slider>().value = audioSliderValue[0];
        audioSliderObjects[1].GetComponent<Slider>().value = audioSliderValue[1];
        audioSliderObjects[2].GetComponent<Slider>().value = audioSliderValue[2];

        SaveAll();
    }

    [ContextMenu("DeletoBlue")]
    public void Deleto()
    {
        SaveSystem.DeleteSave();
    }
    private void Update()
    {
        if (lastMousePos != Input.mousePosition)
        {
            Cursor.visible = true;
            mouseVisibilityTimer = 0;
        }
        else
        {
            mouseVisibilityTimer += Time.deltaTime;
            if (mouseVisibilityTimer >= 2)
            {
                Cursor.visible = false;
            }
        }
        lastMousePos = Input.mousePosition;

        yAxis = Sinput.GetAxis("Vertical");
        if (scrollSinglePlayer)
        {
            singlePlayerRect.velocity = new Vector2(0, -yAxis * scrollSpeed * Time.deltaTime);
        }
        if (scrollMultiPlayer)
        {
            multiPlayerRect.velocity = new Vector2(0, -yAxis * scrollSpeed * Time.deltaTime);
        }
    }
    public void ToggleMenues(int id)
    {
        for (int i = 0; i < menuObjects.Length; i++)
        {
            if (i != id)
            {
                menuObjects[i].SetActive(false);
            }
            else
            {
                menuObjects[i].SetActive(true);
            }
        }

        for (int i = 0; i < SettingsObjects.Length; i++)
        {
            SettingsObjects[i].SetActive(false);
        }
    }

    public void ToggleSettings(int id)
    {
        for (int i = 0; i < SettingsObjects.Length; i++)
        {
            if (i != id)
            {
                SettingsObjects[i].SetActive(false);
            }
            else
            {
                SettingsObjects[i].SetActive(true);
            }
        }


        for (int i = 0; i < audioObjects.Length; i++)
        {
            audioObjects[i].SetActive(true);
            audioSliderObjects[i].SetActive(false);
        }
    }

    public void ToggleAudioSettings(int id)
    {
        for (int i = 0; i < audioObjects.Length; i++)
        {
            if (id == i)
            {
                audioSliderObjects[i].SetActive(true);
                audioObjects[i].SetActive(false);

                StartCoroutine(sliderselect(i));
            }
            else
            {
                audioObjects[i].SetActive(true);
                audioSliderObjects[i].SetActive(false);
            }
        }
    }

    public void SetSinglePlayerScroll(bool value)
    {
        scrollSinglePlayer = value;
    }

    public void SetMultiPlayerScroll(bool value)
    {
        scrollMultiPlayer = value;
    }

    public void SetHighScoreScrollView()
    {
        float _height = 0;
        for (int i = 0; i < singlePlayerHighScore.Count; i++)
        {
            if (i < 50)
            {
                singlePlayerEntries[i].GetComponentInChildren<TMP_Text>().text = "#" + (i + 1) + " " + singlePlayerHighScore[i].name + " : " + singlePlayerHighScore[i].score;
            }
        }

        for (int j = 0; j < singlePlayerEntries.Length; j++)
        {
            if (singlePlayerEntries[j].activeSelf)
            {
                _height += singlePlayerEntries[j].GetComponentInChildren<RectTransform>().rect.height;
            }
        }
        _height += singlePlayerRectTransform.gameObject.GetComponent<VerticalLayoutGroup>().padding.top;
        singlePlayerRectTransform.sizeDelta = new Vector2(singlePlayerRectTransform.sizeDelta.x, _height);
        singlePlayerRect.verticalNormalizedPosition = 1;

        for (int i = 0; i < multiPlayerHighScore.Count; i++)
        {
            if (i < 50)
            {
                multiPlayerEntries[i].GetComponentInChildren<TMP_Text>().text = "#" + (i + 1) + " " + multiPlayerHighScore[i].name + " : " + multiPlayerHighScore[i].score;
            }
        }


        _height = 0;
        for (int k = 0; k < multiPlayerEntries.Length; k++)
        {
            if (multiPlayerEntries[k].activeSelf)
            {
                _height += multiPlayerEntries[k].GetComponentInChildren<RectTransform>().rect.height;
            }
        }
        _height += multiPlayerRectTransform.gameObject.GetComponent<VerticalLayoutGroup>().padding.top;
        multiPlayerRectTransform.sizeDelta = new Vector2(multiPlayerRectTransform.sizeDelta.x, _height);
        multiPlayerRect.verticalNormalizedPosition = 1;
    }

    public void LoadScene(int id)
    {
        //SceneManager.LoadScene(id);
        StartCoroutine(LoadSceneAsy(id));
    }
    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }
    public void SetVolume(int id)
    {
        switch (id)
        {
            case 0:
                audioMixer.SetFloat("MasterVolume", Mathf.Log10(audioSliderObjects[id].GetComponent<Slider>().value) * 20);
                audioSliderValue[id] = audioSliderObjects[id].GetComponent<Slider>().value;
                break;
            case 1:
                audioMixer.SetFloat("MusicVolume", Mathf.Log10(audioSliderObjects[id].GetComponent<Slider>().value) * 20);
                audioSliderValue[id] = audioSliderObjects[id].GetComponent<Slider>().value;
                break;
            case 2:
                audioMixer.SetFloat("SFXVolume", Mathf.Log10(audioSliderObjects[id].GetComponent<Slider>().value) * 20);
                audioSliderValue[id] = audioSliderObjects[id].GetComponent<Slider>().value;
                break;
            default:
                break;
        }
        SaveAll();
    }

    public void SetScreenResolution(int id)
    {
        switch (id)
        {
            case 1:
                ResolutionID = id;
                Screen.SetResolution(1920, 1080, (FullScreenMode)ScreenModeEnum);
                break;
            case 2:
                ResolutionID = id;
                Screen.SetResolution(1600, 900, (FullScreenMode)ScreenModeEnum);
                break;
            case 3:
                ResolutionID = id;
                Screen.SetResolution(1024, 576, (FullScreenMode)ScreenModeEnum);
                break;
            case 4:
                ResolutionID = id;
                Screen.SetResolution(1920, 1200, (FullScreenMode)ScreenModeEnum);
                break;
            case 5:
                ResolutionID = id;
                Screen.SetResolution(1680, 1050, (FullScreenMode)ScreenModeEnum);
                break;
            case 6:
                ResolutionID = id;
                Screen.SetResolution(1024, 800, (FullScreenMode)ScreenModeEnum);
                break;
            case 7:
                ResolutionID = id;
                Screen.SetResolution(3840, 2160, (FullScreenMode)ScreenModeEnum);
                break;
            case 8:
                ResolutionID = id;
                Screen.SetResolution(3840, 2400, (FullScreenMode)ScreenModeEnum);
                break;
            case 9:
                ScreenModeEnum = 1;
                Screen.SetResolution(Screen.currentResolution.width,Screen.currentResolution.height,(FullScreenMode)ScreenModeEnum);
                break;
            case 10:
                ScreenModeEnum = 2;
                Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, (FullScreenMode)ScreenModeEnum);
                break;
            case 11:
                ScreenModeEnum = 3;
                Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, (FullScreenMode)ScreenModeEnum);
                break;
            default:
                break;
        }

        ScreenModeEnum = (int)Screen.fullScreenMode;
        SaveAll();
    }

    public void PlaySound(int id)
    {
        switch (id)
        {
            case 1:
                source.PlayOneShot(hoverClip);
                break;
            case 2:
                source.PlayOneShot(clickClip);
                break;
            default:
                break;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void SaveAll()
    {
        SaveSystem.SavePlayer(this);
    }

    IEnumerator sliderselect(int id)
    {
        yield return new WaitForEndOfFrame();
        eveSys.SetSelectedGameObject(audioSliderObjects[id]);
    }


    IEnumerator LoadSceneAsy(int id)
    {
        yield return null;
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(id);
        asyncOperation.allowSceneActivation = false;

        yield return new WaitForSeconds(0.5f);
        while (!asyncOperation.isDone)
        {
            if (asyncOperation.progress >= 0.9f)
            {
                asyncOperation.allowSceneActivation = true;
            }

            yield return null;
        }
    }
}

