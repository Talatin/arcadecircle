﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveSystem 
{
    public static void SavePlayer(MainMenue menu)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.fun";
        FileStream stream = new FileStream(path, FileMode.Create);

        MenuSaveData data = new MenuSaveData(menu);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static MenuSaveData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/player.fun";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            MenuSaveData data = formatter.Deserialize(stream) as MenuSaveData;
            stream.Close();
            return data;
        }
        else
        {
            Debug.LogError("Save file not found in" + path);
            return null;
        }
    }
    public static void DeleteSave()
    {
        string path = Application.persistentDataPath + "/player.fun";
        if (File.Exists(path))
        {
            File.Delete(path);
        }

        DeleteScores();
    }
    public static void SaveScores(GameManager manager)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/scores.fun";
        FileStream stream = new FileStream(path, FileMode.Create);

        HighScoreData data = new HighScoreData(manager);

        formatter.Serialize(stream, data);
        stream.Close();
    }


    public static HighScoreData LoadScores()
    {
        string path = Application.persistentDataPath + "/scores.fun";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            HighScoreData data = formatter.Deserialize(stream) as HighScoreData;
            stream.Close();
            return data;
        }
        else
        {
            Debug.LogError("Save file not found in" + path);
            return null;
        }
    }

    public static void DeleteScores()
    {
        string path = Application.persistentDataPath + "/scores.fun";
        if (File.Exists(path))
        {
            File.Delete(path);
        }
    }

}
