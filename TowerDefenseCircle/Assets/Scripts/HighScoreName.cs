﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class HighScoreName : MonoBehaviour
{
    private char[] Symbols = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0','$','%','&','/' };
    private int SymbolIndex = 0;
    private int letterIndex = 0;
    private string playerName;
    public TMP_Text[] Letters;

    private void OnEnable()
    {
    }
    void Update()
    {
        if (Sinput.GetButtonDownRepeating("Up"))
        {
            if (SymbolIndex < Symbols.Length-1)
            {
                SymbolIndex++;
            }
            else
            {
                SymbolIndex = 0;
            }
            Letters[letterIndex].text = Symbols[SymbolIndex].ToString();
        }
        if (Sinput.GetButtonDownRepeating("Down"))
        {
            if (SymbolIndex > 0)
            {
                SymbolIndex--;
            }
            else
            {
                SymbolIndex = Symbols.Length-1;
            }
            Letters[letterIndex].text = Symbols[SymbolIndex].ToString();
        }



        if (Sinput.GetButtonDown("Submit"))
        {
            
            if (letterIndex < 2)
            {
                Letters[letterIndex].fontStyle = FontStyles.Normal;
                letterIndex++;
                Letters[letterIndex].fontStyle = FontStyles.Bold;
                SymbolIndex = 0;
            }
            else
            {
                Letters[letterIndex].fontStyle = FontStyles.Normal;
                Debug.Log("Bish we done with the name");
                string temp = Letters[0].text + Letters[1].text + Letters[2].text;
                GameManager.Instance.SetHighScore(temp);
            }
        }

    }
}
