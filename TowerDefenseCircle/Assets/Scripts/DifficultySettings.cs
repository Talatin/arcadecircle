﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu][Serializable]
public class DifficultySettings : ScriptableObject
{
    public bool isOnePlayerMode;

    [Header("SinglePlayerSettings")]
    public float minBlockBoiSpawnTime;
    public float minOrbitalBoiSpawnTime;
    public float minMessengerBoiSpawnTime;
    public float minLineBackerBoiSpawnTime;
    [Space]
    public float initialBlockBoiSpawnTime;
    public float initialOrbitalBoiSpawnTime;
    public float initialMessengerBoiSpawnTime;
    public float initialLineBackerBoiSpawnTime;
    [Space]
    public float difficultyIncreasePercentage;
    public float difficultyTimeTillIncrease;

    [Header("MultiPlayerSettings")]
    public float minBlockBoiSpawnTimeTwo;
    public float minOrbitalBoiSpawnTimeTwo;
    public float minMessengerBoiSpawnTimeTwo;
    public float minLineBackerBoiSpawnTimeTwo;
    [Space]
    public float initialBlockBoiSpawnTimeTwo;
    public float initialOrbitalBoiSpawnTimeTwo;
    public float initialMessengerBoiSpawnTimeTwo;
    public float initialLineBackerBoiSpawnTimeTwo;
    [Space]
    public float difficultyIncreasePercentageTwo;
    public float difficultyTimeTillIncreaseTwo;
}
