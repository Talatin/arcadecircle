﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SinputSystems;
public class TowerShooting : MonoBehaviour
{
    public int id;
    public InputDeviceSlot slot;
    [SerializeField] private AudioSource source;
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private Transform firePos;
    [SerializeField] private float firePower;
    [SerializeField] private float fireRate;
    private float currentFireRate;
    [SerializeField] private int projectilePoolSize;
    private bool canFire;
    private List<GameObject> projectilePool = new List<GameObject>();
    private List<Rigidbody2D> projectilePhysicsPool = new List<Rigidbody2D>();
    private bool fireSoundToggle;
    // Start is called before the first frame update
    void Start()
    {
        GameObject tempClean = new GameObject("BulletPool");
        for (int i = 0; i < projectilePoolSize; i++)
        {
            GameObject temp = Instantiate(projectilePrefab, new Vector3((10 + (float)i / 2), 0, 0), Quaternion.identity);
            projectilePool.Add(temp);
            projectilePhysicsPool.Add(temp.GetComponent<Rigidbody2D>());
            temp.transform.parent = tempClean.transform;
            temp.name = "Projectile Nr. " + i;
            temp.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if ((Sinput.GetButton("Fire1", slot) || Input.GetButton("Fire1" + id)) && canFire)
        {
            Fire();
            Fire();
            currentFireRate = 0;
            canFire = false;
        }
        else
        {
            currentFireRate += Time.deltaTime;
            if (fireRate <= currentFireRate)
            {
                currentFireRate = fireRate;
                canFire = true;
            }
        }
    }

    private void Fire()
    {
        source.pitch = Random.Range(0.95f, 1.05f);
        source.Play();
        for (int i = 0; i < projectilePool.Count; i++)
        {
            if (!projectilePool[i].activeSelf)
            {
                projectilePool[i].SetActive(true);
                projectilePool[i].transform.position = firePos.position;
                projectilePool[i].transform.rotation = firePos.rotation;
                projectilePhysicsPool[i].velocity = Vector2.zero;
                projectilePhysicsPool[i].AddForce(firePos.transform.up * firePower, ForceMode2D.Impulse);
                break;
            }
        }
    }


}
