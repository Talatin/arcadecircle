﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SinputSystems;
public class TowerMovement : MonoBehaviour
{
    public InputDeviceSlot slot;
    public int id;
    private Rigidbody2D rb;
    private float xAxis;
    [SerializeField] private float speed;
    [SerializeField] private Transform rotationsCenter;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(Sinput.GetAxis("Horizontal", slot)) > Mathf.Abs(Input.GetAxis("Horizontal" + id)))
        {
            xAxis = Sinput.GetAxis("Horizontal", slot);
        }
        else
        {
            xAxis = Input.GetAxis("Horizontal" + id);
        }
    }

    private void FixedUpdate()
    {
        float temp;
        rot();
        if (Sinput.GetButton("Fire1", slot) || Input.GetButton("Fire1" + id))
            temp = (speed / 2) * xAxis;
        else
            temp = speed * xAxis;

        rb.velocity = transform.right * temp * Time.fixedDeltaTime;
    }

    private void rot()
    {
        Vector3 diff = rotationsCenter.position - transform.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z + 90);
    }
}
