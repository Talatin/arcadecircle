﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MenuSaveData
{
    public float[] Volumes;
    public int screenResID;
    public int screenModeEnum;

    public MenuSaveData(MainMenue menu)
    {
        Volumes = new float[3];
        Volumes[0] = menu.audioSliderValue[0];
        Volumes[1] = menu.audioSliderValue[1];
        Volumes[2] = menu.audioSliderValue[2];

        screenResID = menu.ResolutionID;
        screenModeEnum = menu.ScreenModeEnum;

    }

}

[System.Serializable]
public class HighScoreData
{
    public List<ScoreEntry> SinglePlayerScores;
    public List<ScoreEntry> MultiplayerScores;


    public HighScoreData(GameManager manager)
    {
        SinglePlayerScores = manager.singles;
        MultiplayerScores = manager.multies;
    }

}