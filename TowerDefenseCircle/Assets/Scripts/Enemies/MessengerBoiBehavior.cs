﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessengerBoiBehavior : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 startingPosition;
    private EnemieSpawning spawner;

    private void Start()
    {
        spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<EnemieSpawning>();
    }

    private void OnEnable()
    {
        startingPosition = transform.position;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Base"))
        {
            spawner.SpawnBigBoi(startingPosition);
        }
    }
}
