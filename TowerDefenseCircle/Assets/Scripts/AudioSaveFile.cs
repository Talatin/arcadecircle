﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu][Serializable]
public class AudioSaveFile : ScriptableObject
{

    public float MasterVolume;
    public float MusicVolume;
    public float SFXVolume;

    public int screenResoID;
    public FullScreenMode screenMode;


}
