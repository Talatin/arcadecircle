﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineBackerBehavior : MonoBehaviour
{


    [SerializeField] private Collider2D Playerblock;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private EnemyMovement movement;
    [SerializeField] private SpriteRenderer spRend;
    [SerializeField] private Animator anim;
    [SerializeField] private int health;
    [SerializeField] private int attackPower;
    private int baseHealth;
    private TrailRenderer trail;
    private void Start()
    {
        trail = GetComponent<TrailRenderer>();
        movement = GetComponent<EnemyMovement>();
        baseHealth = health;
    }

    private void FixedUpdate()
    {
        Rotate();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Base"))
        {
            collision.gameObject.GetComponent<BaseBehavior>().Damage(attackPower);
            rb.bodyType = RigidbodyType2D.Kinematic;
            movement.enabled = false;
            spRend.enabled = false;
            Playerblock.enabled = true;
            Invoke("SelfDestruct", trail.time);
        }
    }
    private void Rotate()
    {
        Vector3 diff = Vector3.zero - transform.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z);
    }

    private void SelfDestruct()
    {
        gameObject.SetActive(false);
    }

    public void Damage(int amount)
    {
        anim.SetTrigger("Damage");
        health -= amount;
        if (health <= 0)
        {
            gameObject.SetActive(false);
            health = baseHealth;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Projectile"))
        {
            Damage(1);
        }
    }

    private void OnEnable()
    {
        if (trail != null)
        {
            trail.Clear();
        }
        Playerblock.enabled = false;
        rb.bodyType = RigidbodyType2D.Dynamic;
        movement.enabled = true;
        spRend.enabled = true;
    }




}
